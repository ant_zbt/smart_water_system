package com.ruoyi.project.water.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 充值记录对象 water_pay
 * 
 * @author 16软外
 * @date 2020-04-21
 */
public class WaterPay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Integer id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用水表id */
    @Excel(name = "用水表id")
    private Long usageId;

    /** 充值/交费金额 */
    @Excel(name = "充值/交费金额")
    private Double amount;

    /** 充值/交费时间 */
    @Excel(name = "充值/交费时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 充值/交费状态 */
    @Excel(name = "充值/交费状态")
    private String status;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }
    public void setUsageId(Long usageId)
    {
        this.usageId = usageId;
    }

    public Long getUsageId()
    {
        return usageId;
    }
    public void setAmount(Double amount) 
    {
        this.amount = amount;
    }

    public Double getAmount() 
    {
        return amount;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("usageId", getUsageId())
            .append("amount", getAmount())
            .append("payTime", getPayTime())
            .append("status", getStatus())
            .append("type", getType())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .toString();
    }
}
