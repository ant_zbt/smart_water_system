package com.ruoyi.project.water.pay.service;

import com.ruoyi.project.water.pay.domain.WaterPay;

import java.util.List;

/**
 * 充值记录Service接口
 * 
 * @author 16软外
 * @date 2020-04-21
 */
public interface IWaterPayService 
{
    /**
     * 查询充值记录
     * 
     * @param id 充值记录ID
     * @return 充值记录
     */
    public WaterPay selectWaterPayById(Integer id);

    /**
     * 查询充值记录列表
     * 
     * @param waterPay 充值记录
     * @return 充值记录集合
     */
    public List<WaterPay> selectWaterPayList(WaterPay waterPay);

    /**
     * 新增充值记录
     * 
     * @param waterPay 充值记录
     * @return 结果
     */
    public int insertWaterPay(WaterPay waterPay);

    /**
     * 修改充值记录
     * 
     * @param waterPay 充值记录
     * @return 结果
     */
    public int updateWaterPay(WaterPay waterPay);

    /**
     * 批量删除充值记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWaterPayByIds(String ids);

    /**
     * 删除充值记录信息
     * 
     * @param id 充值记录ID
     * @return 结果
     */
    public int deleteWaterPayById(Integer id);
}
