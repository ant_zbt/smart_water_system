package com.ruoyi.project.water.pay.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import com.ruoyi.project.water.pay.domain.WaterPay;
import com.ruoyi.project.water.pay.service.IWaterPayService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 充值记录Controller
 * 
 * @author 16软外
 * @date 2020-04-21
 */
@Controller
@RequestMapping("/water/pay")
public class WaterPayController extends BaseController
{
    private String prefix = "water/pay";

    @Autowired
    private IWaterPayService waterPayService;

    @Autowired
    private IUserService userService;

    @RequiresPermissions("water:pay:view")
    @GetMapping()
    public String pay(ModelMap mmap)
    {
        mmap.put("userId", getUserId());
        return prefix + "/pay";
    }

    /**
     * 查询充值记录列表
     */
    @RequiresPermissions("water:pay:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WaterPay waterPay)
    {
        startPage();
        List<WaterPay> list = waterPayService.selectWaterPayList(waterPay);
        return getDataTable(list);
    }

    /**
     * 导出充值记录列表
     */
    @RequiresPermissions("water:pay:export")
    @Log(title = "充值记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WaterPay waterPay)
    {
        List<WaterPay> list = waterPayService.selectWaterPayList(waterPay);
        ExcelUtil<WaterPay> util = new ExcelUtil<WaterPay>(WaterPay.class);
        return util.exportExcel(list, "pay");
    }

    /**
     * 新增充值记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存充值记录
     */
    @RequiresPermissions("water:pay:add")
    @Log(title = "充值记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WaterPay waterPay)
    {
        return toAjax(waterPayService.insertWaterPay(waterPay));
    }

    /**
     * 修改充值记录
     */
    @GetMapping("/edit")
    public String edit(ModelMap mmap)
    {
        User user = userService.selectUserById(getUserId());
        mmap.put("user", user);
        return prefix + "/edit";
    }

    /**
     * 修改保存充值记录
     */
    @RequiresPermissions("water:pay:edit")
    @Log(title = "充值金额", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WaterPay waterPay)
    {
        User user = userService.selectUserById(getUserId());
        user.setBalance(user.getBalance() + waterPay.getAmount());
        waterPay.setPayTime(new Date());
        waterPay.setUsageId(0L);
        waterPay.setStatus("成功");
        waterPay.setType("在线充值");
        waterPayService.insertWaterPay(waterPay);
        return toAjax(userService.updateUser(user));
    }

    /**
     * 删除充值记录
     */
    @RequiresPermissions("water:pay:remove")
    @Log(title = "充值记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(waterPayService.deleteWaterPayByIds(ids));
    }
}
