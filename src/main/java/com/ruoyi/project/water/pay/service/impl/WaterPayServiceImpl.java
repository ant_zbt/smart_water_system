package com.ruoyi.project.water.pay.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.water.pay.domain.WaterPay;
import com.ruoyi.project.water.pay.mapper.WaterPayMapper;
import com.ruoyi.project.water.pay.service.IWaterPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 充值记录Service业务层处理
 * 
 * @author 16软外
 * @date 2020-04-21
 */
@Service
public class WaterPayServiceImpl implements IWaterPayService 
{
    @Autowired
    private WaterPayMapper waterPayMapper;

    /**
     * 查询充值记录
     * 
     * @param id 充值记录ID
     * @return 充值记录
     */
    @Override
    public WaterPay selectWaterPayById(Integer id)
    {
        return waterPayMapper.selectWaterPayById(id);
    }

    /**
     * 查询充值记录列表
     * 
     * @param waterPay 充值记录
     * @return 充值记录
     */
    @Override
    public List<WaterPay> selectWaterPayList(WaterPay waterPay)
    {
        return waterPayMapper.selectWaterPayList(waterPay);
    }

    /**
     * 新增充值记录
     * 
     * @param waterPay 充值记录
     * @return 结果
     */
    @Override
    public int insertWaterPay(WaterPay waterPay)
    {
        waterPay.setCreateTime(DateUtils.getNowDate());
        return waterPayMapper.insertWaterPay(waterPay);
    }

    /**
     * 修改充值记录
     * 
     * @param waterPay 充值记录
     * @return 结果
     */
    @Override
    public int updateWaterPay(WaterPay waterPay)
    {
        return waterPayMapper.updateWaterPay(waterPay);
    }

    /**
     * 删除充值记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWaterPayByIds(String ids)
    {
        return waterPayMapper.deleteWaterPayByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除充值记录信息
     * 
     * @param id 充值记录ID
     * @return 结果
     */
    @Override
    public int deleteWaterPayById(Integer id)
    {
        return waterPayMapper.deleteWaterPayById(id);
    }
}
