package com.ruoyi.project.water.usage.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import com.ruoyi.project.water.usage.domain.WaterUsage;
import com.ruoyi.project.water.usage.service.IWaterUsageService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 日用水量Controller
 * 
 * @author 16软外
 * @date 2020-04-20
 */
@Controller
@RequestMapping("/water/usage")
public class WaterUsageController extends BaseController
{
    private String prefix = "water/usage";

    @Autowired
    private IWaterUsageService waterUsageService;

    @Autowired
    private IUserService userService;

    @RequiresPermissions("water:usage:view")
    @GetMapping()
    public String usage()
    {
        return prefix + "/usage";
    }

    @GetMapping("/pay")
    public String pay(ModelMap mmap)
    {
        mmap.put("userId", getUserId());
        return prefix + "/usageUser";
    }

    /**
     * 查询日用水量列表
     */
    @RequiresPermissions("water:usage:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WaterUsage waterUsage)
    {
        startPage();
        List<WaterUsage> list = waterUsageService.selectWaterUsageList(waterUsage);
        return getDataTable(list);
    }

    /**
     * 导出日用水量列表
     */
    @RequiresPermissions("water:usage:export")
    @Log(title = "日用水量", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WaterUsage waterUsage)
    {
        List<WaterUsage> list = waterUsageService.selectWaterUsageList(waterUsage);
        ExcelUtil<WaterUsage> util = new ExcelUtil<WaterUsage>(WaterUsage.class);
        return util.exportExcel(list, "usage");
    }

    /**
     * 新增日用水量
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存日用水量
     */
    @RequiresPermissions("water:usage:add")
    @Log(title = "日用水量", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WaterUsage waterUsage)
    {
        User user = userService.selectUserByLoginName(waterUsage.getLoginName());
        if (user == null) {
            return error("该户号不存在!");
        }
        waterUsage.setPayStatus("未交费");
        waterUsage.setUserId(user.getUserId());
        return toAjax(waterUsageService.insertWaterUsage(waterUsage));
    }

    /**
     * 修改日用水量
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WaterUsage waterUsage = waterUsageService.selectWaterUsageById(id);
        mmap.put("waterUsage", waterUsage);
        return prefix + "/edit";
    }

    /**
     * 修改保存日用水量
     */
    @RequiresPermissions("water:usage:edit")
    @Log(title = "日用水量", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WaterUsage waterUsage)
    {
        return toAjax(waterUsageService.updateWaterUsage(waterUsage));
    }

    /**
     * 删除日用水量
     */
    @RequiresPermissions("water:usage:remove")
    @Log(title = "日用水量", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(waterUsageService.deleteWaterUsageByIds(ids));
    }

    /**
     * 用户交费
     */
    @RequiresPermissions("water:usage:pay")
    @Log(title = "日用水量", businessType = BusinessType.DELETE)
    @PostMapping( "/pay")
    @ResponseBody
    public AjaxResult pay(String ids)
    {
        return waterUsageService.payWaterUsageByIds(ids);
    }
}
