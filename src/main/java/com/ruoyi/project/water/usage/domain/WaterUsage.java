package com.ruoyi.project.water.usage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 日用水量对象 water_usage
 * 
 * @author 16软外
 * @date 2020-04-20
 */
public class WaterUsage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用水量 */
    @Excel(name = "用水量")
    private Double waterDemand;

    /** 用水金额 */
    @Excel(name = "用水金额")
    private Double waterAmount;

    /**
     * 登录名称
     */
    @Excel(name = "户号")
    private String loginName;

    /**
     * 用户名称
     */
    @Excel(name = "户名")
    private String userName;

    /**
     * 手机号码
     */
    @Excel(name = "手机号码")
    private String phonenumber;

    @Excel(name = "余额")
    private Double balance;

    @Excel(name = "用户单位")
    private String unit;

    @Excel(name = "用户地址")
    private String address;

    @Excel(name = "缴费状态")
    private String payStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setWaterDemand(Double waterDemand) 
    {
        this.waterDemand = waterDemand;
    }

    public Double getWaterDemand() 
    {
        return waterDemand;
    }
    public void setWaterAmount(Double waterAmount) 
    {
        this.waterAmount = waterAmount;
    }

    public Double getWaterAmount() 
    {
        return waterAmount;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("waterDemand", getWaterDemand())
            .append("createTime", getCreateTime())
            .append("waterAmount", getWaterAmount())
            .toString();
    }
}
