package com.ruoyi.project.water.usage.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.system.user.mapper.UserMapper;
import com.ruoyi.project.water.pay.domain.WaterPay;
import com.ruoyi.project.water.pay.mapper.WaterPayMapper;
import com.ruoyi.project.water.usage.domain.WaterUsage;
import com.ruoyi.project.water.usage.mapper.WaterUsageMapper;
import com.ruoyi.project.water.usage.service.IWaterUsageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 日用水量Service业务层处理
 * 
 * @author 16软外
 * @date 2020-04-20
 */
@Service
public class WaterUsageServiceImpl implements IWaterUsageService 
{
    @Autowired
    private WaterUsageMapper waterUsageMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WaterPayMapper waterPayMapper;

    /**
     * 查询日用水量
     * 
     * @param id 日用水量ID
     * @return 日用水量
     */
    @Override
    public WaterUsage selectWaterUsageById(Long id)
    {
        return waterUsageMapper.selectWaterUsageById(id);
    }

    /**
     * 查询日用水量列表
     * 
     * @param waterUsage 日用水量
     * @return 日用水量
     */
    @Override
    public List<WaterUsage> selectWaterUsageList(WaterUsage waterUsage)
    {
        return waterUsageMapper.selectWaterUsageList(waterUsage);
    }

    /**
     * 新增日用水量
     * 
     * @param waterUsage 日用水量
     * @return 结果
     */
    @Override
    public int insertWaterUsage(WaterUsage waterUsage)
    {
        waterUsage.setCreateTime(DateUtils.getNowDate());
        return waterUsageMapper.insertWaterUsage(waterUsage);
    }

    /**
     * 修改日用水量
     * 
     * @param waterUsage 日用水量
     * @return 结果
     */
    @Override
    public int updateWaterUsage(WaterUsage waterUsage)
    {
        return waterUsageMapper.updateWaterUsage(waterUsage);
    }

    /**
     * 删除日用水量对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWaterUsageByIds(String ids)
    {
        return waterUsageMapper.deleteWaterUsageByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除日用水量信息
     * 
     * @param id 日用水量ID
     * @return 结果
     */
    @Override
    public int deleteWaterUsageById(Long id)
    {
        return waterUsageMapper.deleteWaterUsageById(id);
    }

    /**
     * 用户交费
     *
     * @param ids 日用水量ID
     * @return 结果
     */
    @Override
    public AjaxResult payWaterUsageByIds(String ids)
    {
        String[] strings = Convert.toStrArray(ids);
        for (String id : strings) {
            long usageId = Long.valueOf(id);
            WaterUsage waterUsage = waterUsageMapper.selectWaterUsageById(usageId);
            if (waterUsage.getPayStatus().equals("已交费")) {
                return AjaxResult.error("该条信息已交费, 无需重复交费!");
            }
            WaterPay waterPay = new WaterPay();
            waterPay.setType("在线交费");
            waterPay.setUsageId(usageId);
            waterPay.setStatus("成功");
            waterPay.setUserId(waterUsage.getUserId());
            waterPay.setPayTime(new Date());
            waterPay.setAmount(waterUsage.getWaterAmount());
            waterPayMapper.insertWaterPay(waterPay);
            waterUsage.setPayStatus("已交费");
            waterUsageMapper.updateWaterUsage(waterUsage);
        }
        return AjaxResult.success();
    }
}
