package com.ruoyi.project.water.usage.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.water.usage.domain.WaterUsage;

import java.util.List;

/**
 * 日用水量Service接口
 * 
 * @author 16软外
 * @date 2020-04-20
 */
public interface IWaterUsageService 
{
    /**
     * 查询日用水量
     * 
     * @param id 日用水量ID
     * @return 日用水量
     */
    public WaterUsage selectWaterUsageById(Long id);

    /**
     * 查询日用水量列表
     * 
     * @param waterUsage 日用水量
     * @return 日用水量集合
     */
    public List<WaterUsage> selectWaterUsageList(WaterUsage waterUsage);

    /**
     * 新增日用水量
     * 
     * @param waterUsage 日用水量
     * @return 结果
     */
    public int insertWaterUsage(WaterUsage waterUsage);

    /**
     * 修改日用水量
     * 
     * @param waterUsage 日用水量
     * @return 结果
     */
    public int updateWaterUsage(WaterUsage waterUsage);

    /**
     * 批量删除日用水量
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWaterUsageByIds(String ids);

    /**
     * 用户交费
     *
     * @param ids
     * @return 结果
     */
    public AjaxResult payWaterUsageByIds(String ids);

    /**
     * 删除日用水量信息
     * 
     * @param id 日用水量ID
     * @return 结果
     */
    public int deleteWaterUsageById(Long id);
}
