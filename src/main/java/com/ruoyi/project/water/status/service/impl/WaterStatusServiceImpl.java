package com.ruoyi.project.water.status.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.water.status.mapper.WaterStatusMapper;
import com.ruoyi.project.water.status.domain.WaterStatus;
import com.ruoyi.project.water.status.service.IWaterStatusService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 抄表控阀Service业务层处理
 * 
 * @author 16软外
 * @date 2020-04-22
 */
@Service
public class WaterStatusServiceImpl implements IWaterStatusService 
{
    @Autowired
    private WaterStatusMapper waterStatusMapper;

    /**
     * 查询抄表控阀
     * 
     * @param id 抄表控阀ID
     * @return 抄表控阀
     */
    @Override
    public WaterStatus selectWaterStatusById(Long id)
    {
        return waterStatusMapper.selectWaterStatusById(id);
    }

    /**
     * 查询抄表控阀列表
     * 
     * @param waterStatus 抄表控阀
     * @return 抄表控阀
     */
    @Override
    public List<WaterStatus> selectWaterStatusList(WaterStatus waterStatus)
    {
        return waterStatusMapper.selectWaterStatusList(waterStatus);
    }

    /**
     * 新增抄表控阀
     * 
     * @param waterStatus 抄表控阀
     * @return 结果
     */
    @Override
    public int insertWaterStatus(WaterStatus waterStatus)
    {
        waterStatus.setCreateTime(DateUtils.getNowDate());
        return waterStatusMapper.insertWaterStatus(waterStatus);
    }

    /**
     * 修改抄表控阀
     * 
     * @param waterStatus 抄表控阀
     * @return 结果
     */
    @Override
    public int updateWaterStatus(WaterStatus waterStatus)
    {
        return waterStatusMapper.updateWaterStatus(waterStatus);
    }

    /**
     * 删除抄表控阀对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteWaterStatusByIds(String ids)
    {
        return waterStatusMapper.deleteWaterStatusByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除抄表控阀信息
     * 
     * @param id 抄表控阀ID
     * @return 结果
     */
    @Override
    public int deleteWaterStatusById(Long id)
    {
        return waterStatusMapper.deleteWaterStatusById(id);
    }
}
