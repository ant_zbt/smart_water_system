package com.ruoyi.project.water.status.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import com.ruoyi.project.water.status.domain.WaterStatus;
import com.ruoyi.project.water.status.service.IWaterStatusService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 抄表控阀Controller
 * 
 * @author 16软外
 * @date 2020-04-22
 */
@Controller
@RequestMapping("/water/status")
public class WaterStatusController extends BaseController
{
    private String prefix = "water/status";

    @Autowired
    private IWaterStatusService waterStatusService;

    @Autowired
    private IUserService userService;

    @RequiresPermissions("water:status:view")
    @GetMapping()
    public String status()
    {
        return prefix + "/status";
    }

    /**
     * 查询抄表控阀列表
     */
    @RequiresPermissions("water:status:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WaterStatus waterStatus)
    {
        startPage();
        List<WaterStatus> list = waterStatusService.selectWaterStatusList(waterStatus);
        return getDataTable(list);
    }

    /**
     * 导出抄表控阀列表
     */
    @RequiresPermissions("water:status:export")
    @Log(title = "抄表控阀", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WaterStatus waterStatus)
    {
        List<WaterStatus> list = waterStatusService.selectWaterStatusList(waterStatus);
        ExcelUtil<WaterStatus> util = new ExcelUtil<WaterStatus>(WaterStatus.class);
        return util.exportExcel(list, "status");
    }

    /**
     * 新增抄表控阀
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存抄表控阀
     */
    @RequiresPermissions("water:status:add")
    @Log(title = "抄表控阀", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WaterStatus waterStatus)
    {
        User user = userService.selectUserByLoginName(waterStatus.getLoginName());
        if (user == null) {
            return error("该户号不存在!");
        }
        waterStatus.setUserId(user.getUserId());
        return toAjax(waterStatusService.insertWaterStatus(waterStatus));
    }

    /**
     * 修改抄表控阀
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WaterStatus waterStatus = waterStatusService.selectWaterStatusById(id);
        mmap.put("waterStatus", waterStatus);
        return prefix + "/edit";
    }

    /**
     * 修改保存抄表控阀
     */
    @RequiresPermissions("water:status:edit")
    @Log(title = "抄表控阀", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WaterStatus waterStatus)
    {
        return toAjax(waterStatusService.updateWaterStatus(waterStatus));
    }

    /**
     * 删除抄表控阀
     */
    @RequiresPermissions("water:status:remove")
    @Log(title = "抄表控阀", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(waterStatusService.deleteWaterStatusByIds(ids));
    }
}
