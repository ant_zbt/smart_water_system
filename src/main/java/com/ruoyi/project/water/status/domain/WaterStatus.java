package com.ruoyi.project.water.status.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 抄表控阀对象 water_status
 * 
 * @author 16软外
 * @date 2020-04-22
 */
public class WaterStatus extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 水表状态 */
    @Excel(name = "水表状态")
    private String meterStauts;

    /** 门阀状态 */
    @Excel(name = "门阀状态")
    private String valveStatus;

    @Excel(name = "余额")
    private Double balance;
    /**
     * 登录名称
     */
    @Excel(name = "户号")
    private String loginName;

    /**
     * 用户名称
     */
    @Excel(name = "户名")
    private String userName;

    /**
     * 手机号码
     */
    @Excel(name = "手机号码")
    private String phonenumber;

    @Excel(name = "用户单位")
    private String unit;

    @Excel(name = "用户地址")
    private String address;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setMeterStauts(String meterStauts) 
    {
        this.meterStauts = meterStauts;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getMeterStauts()
    {
        return meterStauts;
    }
    public void setValveStatus(String valveStatus) 
    {
        this.valveStatus = valveStatus;
    }

    public String getValveStatus() 
    {
        return valveStatus;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("meterStauts", getMeterStauts())
            .append("createTime", getCreateTime())
            .append("valveStatus", getValveStatus())
            .toString();
    }
}
