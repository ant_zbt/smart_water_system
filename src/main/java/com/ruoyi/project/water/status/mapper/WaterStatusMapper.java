package com.ruoyi.project.water.status.mapper;

import java.util.List;
import com.ruoyi.project.water.status.domain.WaterStatus;

/**
 * 抄表控阀Mapper接口
 * 
 * @author 16软外
 * @date 2020-04-22
 */
public interface WaterStatusMapper 
{
    /**
     * 查询抄表控阀
     * 
     * @param id 抄表控阀ID
     * @return 抄表控阀
     */
    public WaterStatus selectWaterStatusById(Long id);

    /**
     * 查询抄表控阀列表
     * 
     * @param waterStatus 抄表控阀
     * @return 抄表控阀集合
     */
    public List<WaterStatus> selectWaterStatusList(WaterStatus waterStatus);

    /**
     * 新增抄表控阀
     * 
     * @param waterStatus 抄表控阀
     * @return 结果
     */
    public int insertWaterStatus(WaterStatus waterStatus);

    /**
     * 修改抄表控阀
     * 
     * @param waterStatus 抄表控阀
     * @return 结果
     */
    public int updateWaterStatus(WaterStatus waterStatus);

    /**
     * 删除抄表控阀
     * 
     * @param id 抄表控阀ID
     * @return 结果
     */
    public int deleteWaterStatusById(Long id);

    /**
     * 批量删除抄表控阀
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWaterStatusByIds(String[] ids);
}
